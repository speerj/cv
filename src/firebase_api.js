import { initializeApp } from 'https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js'
import {
    initializeAppCheck,
    ReCaptchaV3Provider,
} from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app-check.js"
import { getVertexAI, getGenerativeModel } from 'https://www.gstatic.com/firebasejs/10.12.2/firebase-vertexai-preview.js'

const firebaseConfig = {

    apiKey: "AIzaSyDzUzbHOlw1PJxdrIQNerUPIMpCijLHSE8",

    authDomain: "resume-9aa7b.firebaseapp.com",

    projectId: "resume-9aa7b",

    storageBucket: "resume-9aa7b.appspot.com",

    messagingSenderId: "863635043598",

    appId: "1:863635043598:web:34f72ea103df105cd9535f"

};

const RECAPTCHA_SITE_KEY = "6LcPHw4qAAAAAGNltskWMAayBPWY-_sIM45brNdW"


// Initialize Firebase

const app = initializeApp(firebaseConfig);

const appCheck = initializeAppCheck(app, {
    provider: new ReCaptchaV3Provider(RECAPTCHA_SITE_KEY),
  });;

const vertexAi = getVertexAI(app)

const model = getGenerativeModel(vertexAi, { model: "gemini-1.5-flash" })

async function callApi() {
    const textBox = document.getElementById('chatbox-input');
    const textBoxOutput = document.getElementById('chatbox-messages');
    const textPart = document.getElementById('raw-md');
    const prompt = "The first item surrounded by three single quotes is a job description, the second is Johann's resume. Please provide a brief description with bullet points of where this person's skill align with the job description. Try to make it succinct in the point for busy recruiters. Here is the job description: '''"+
     textBox.value +
      "'''. Here is the resume: " + 
      "'''" + textPart.innerText + 
      "'''"
    const result = await model.generateContent(prompt)
    textBoxOutput.value = result.response.text()
    console.log(textBoxOutput.scrollHeight);
    textBoxOutput.style.height = textBoxOutput.scrollHeight + "px";
}

document.addEventListener("DOMContentLoaded", function () {
    window.callApi = callApi;
 })