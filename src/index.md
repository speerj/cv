---
hide:
  - navigation
---
<script type="module" src="firebase_api.js"> </script>

{!templates/0-intro.md!}

{!templates/comparor.md!}
{!templates/1-experiences.md!}

{!templates/2-jobs.md!}

----
<div class="show-on-print"> Check out the interactive web version! <a href="https://speerj.gitlab.io/cv/"> https://speerj.gitlab.io/cv/ </a></div>
Check out the code for this!: [https://gitlab.com/speerj/cv](https://gitlab.com/speerj/cv)

<div id="raw-md" hidden> 
{!templates/1-experiences.md!}
{!templates/2-jobs.md!}
</div>

