
# Johann Speer
<svg xmlns="http://www.w3.org/2000/svg" width="90" height="20"><linearGradient id="a" x2="0" y2="100%"><stop offset="0" stop-color="#bbb" stop-opacity=".1"/><stop offset="1" stop-opacity=".1"/></linearGradient><rect rx="3" width="90" height="20" fill="#555"/><rect rx="3" x="37" width="53" height="20" fill="#4c1"/><path fill="#4c1" d="M37 0h4v20h-4z"/><rect rx="3" width="90" height="20" fill="url(#a)"/><g fill="#fff" text-anchor="middle" font-family="DejaVu Sans,Verdana,Geneva,sans-serif" font-size="11"><text x="19.5" y="15" fill="#010101" fill-opacity=".3">build</text><text x="19.5" y="14">build</text><text x="62.5" y="15" fill="#010101" fill-opacity=".3">passing</text><text x="62.5" y="14">passing</text></g></svg> <svg fill="currentColor" style="color: white; margin-left: 20px" height="15"  width="16" > <path fill-rule="evenodd" d="M5 3.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm0 2.122a2.25 2.25 0 10-1.5 0v.878A2.25 2.25 0 005.75 8.5h1.5v2.128a2.251 2.251 0 101.5 0V8.5h1.5a2.25 2.25 0 002.25-2.25v-.878a2.25 2.25 0 10-1.5 0v.878a.75.75 0 01-.75.75h-4.5A.75.75 0 015 6.25v-.878zm3.75 7.378a.75.75 0 11-1.5 0 .75.75 0 011.5 0zm3-8.75a.75.75 0 100-1.5.75.75 0 000 1.5z"></path></svg> 2 forks </br>

------
+1 989 634 1075 </br> 
<a> https://www.linkedin.com/in/johann-speer-93116994/ </a><br/>
johann.speer@proton.me

### Software Developer
------
#### In Short:

- Proven problem-solver and communicator.
- 11 Years of professional software development experience, mostly as a full-stack developer using frameworks such as Flutter, Angular, and Ruby on Rails.
- Fluent in Ruby and Dart. Comfortable with Javascript, SQL, and CSS.
- Created an internal tool to emulate a Bluetooth device. This allows immediate feedback on code changes and frees 20+ minutes of developer time daily.

------
