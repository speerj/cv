###Jobs:
----

#### Android Developer Airborne Athletics | Independent Contractor | November 2021 to March 2024

At Airborne Athletics, I tested, developed, and refactored parts of the flagship Flutter app and collaborated with project managers to improve the process.
 - [x] Led the transition of the Flutter app and the multiple custom libraries to null-safety.
 - [x] Created and designed numerous tools to emulate the physical device, allowing integration testing and immediate developer feedback.
 - [x] Led the development of the integration, widget and unit testing suites.
 - [x] Came up with and implemented projects to remove dialogs for UX improvement.
 - [x] Initiated, advocated, and developed transitions to CI/CD, branch-based development, and more incremental/agile practices.
 - [x] Wrote metaprogramming code generation using source_gen and build_runner.

---

#### Ruby Developer Best Buy | Randstad Contractor | March 2019 to June 2021

At Best Buy, on the infrauto team, I collaborated with stakeholders and project managers to add new features, monitor existing systems, and make incremental improvements on our Ruby-based infrastructure team.

- [x] Worked on a transition from Angular JS to Angular. 
- [x] Implemented CI/CD solutions in BitBucket.
- [x] Rapidly increased the speed and coverage of tests in a large Ruby CLI app.
- [x] Added AWS S3 support to a Ruby on Rails Project.
- [x] Took night-shifts monitoring analytics of critical systems during Thanksgiving/Black Friday rushes.
- [x] Worked with various technologies, including RabbitMQ, AWS, and Openshift.
- [x] Worked with NoSql and Relational DBs.


---

#### Software Developer Heartland America | July 2012 to October 2018 

At Heartland America, I started in November 2010 as a warehouse worker and was asked to help work on a PHP project. Later, as a developer, I was given a diverse set of responsibilities. I was responsible for conceiving, producing, monitoring, and maintaining a wide array of Ruby on Rails-based tools. We used these tools to optimize internal systems.

- [x] Introduced git to the development team.
- [x] Built an Android app in Java for barcode reading and setting orders as shipped.
- [x] Led and gave direction to the junior developer.
- [x] Wrote a listing poster from our OMS to eBay, initially in PHP and then in Ruby on Rails.
- [x] Developed an Active Directory environment for the call center.
- [x] Owned and developed a screen scraping application in C# for the old terminal-based OMS.
- [x] Owned and developed an invoice-generating web app in Rails.
- [x] Worked heavily on address validation with C#, and F#.
- [x] Worked with MySql, SQL Server, and PostgreSQL.
- [x] Maintained Linux and AIX servers.

### Additional Professional Activities:
#### Online Ruby on Rails Tutoring (Freelance) June 2024 - Present
- Provide instruction and grow as a teacher to improve soft-skills and help students
- Maintained and enhanced Ruby on Rails skills in Active Storage, Turbo / Stimulus, and other topics