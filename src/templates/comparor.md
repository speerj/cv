<div class="no-print">
    <h4> Use AI to see how my skills compare to your job description: </h4>
    <div class="chatbox no-print">
        <textarea id="chatbox-messages" readonly></textarea>
        <textarea id="chatbox-input" placeholder="Enter your job description here: (up to 3000 characters)" maxlength="3000" class="input-box" ></textarea>
        <button type="button" id="chatbox-send" onclick="callApi()">Submit</button>
    </div>
</div>